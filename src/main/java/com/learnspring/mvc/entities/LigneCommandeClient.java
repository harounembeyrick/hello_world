package com.learnspring.mvc.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class LigneCommandeClient implements Serializable{

	@Id
	@GeneratedValue
	private Long idLigneComdClient;
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	
	@ManyToOne
	@JoinColumn(name="idComClient")
	private CommandeClient commandeClient;

	
	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeClient getComdclient() {
		return commandeClient;
	}

	public void setComdclient(CommandeClient comdclient) {
		this.commandeClient = comdclient;
	}

	public void setIdLigneComdClient(Long idLigneComdClient) {
		this.idLigneComdClient = idLigneComdClient;
	}

	public Long getIdLigneComdClient() {
		return idLigneComdClient;
	}

	public void setIdArticle(long idLigneComdClient) {
		this.idLigneComdClient = idLigneComdClient;
	}
}
