package com.learnspring.mvc.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Category implements Serializable{

	@Id
	@GeneratedValue
	private Long idCategory;
	private String code;
	private String designation;
	

	@OneToMany(mappedBy="category")
    private List<Article> articles;
    
	public Category() {
	}
    
	
	public Long getIdCategory() {
		return idCategory;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	public void setIdArticle(long idCategory) {
		this.idCategory = idCategory;
	}
}
