package com.learnspring.mvc.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class MvtStck implements Serializable{
    public static final int ENTREE=1;
    public static final int SORTIE=1;
    
	@Id
	@GeneratedValue
	private Long idMvtStck;
	
	private Date datemvt;
	private BigDecimal quantite;
	
	private int typemvt;
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public Date getDatemvt() {
		return datemvt;
	}

	public void setDatemvt(Date datemvt) {
		this.datemvt = datemvt;
	}

	public BigDecimal getQuantite() {
		return quantite;
	}

	public void setQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}

	public int getTypemvt() {
		return typemvt;
	}

	public void setTypemvt(int typemvt) {
		this.typemvt = typemvt;
	}

	public void setIdMvtStck(Long idMvtStck) {
		this.idMvtStck = idMvtStck;
	}

	public Long getIdMvtStck() {
		return idMvtStck;
	}

	public void setIdArticle(Long id) {
		this.idMvtStck = id;
	}
}
