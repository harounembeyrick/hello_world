package com.learnspring.mvc.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Vente implements Serializable{

	@Id
	@GeneratedValue
	private Long idVente;
	
	private String Code;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateVente;
	
	
	@OneToMany(mappedBy="vente")
	private List<LigneVente> ligneVentes;

	public String getCode() {
		return Code;
	}

	public void setCode(String code) {
		Code = code;
	}

	public Date getDateVente() {
		return dateVente;
	}

	public void setDateVente(Date dateVente) {
		this.dateVente = dateVente;
	}

	public List<LigneVente> getLigneVentes() {
		return ligneVentes;
	}

	public void setLigneVentes(List<LigneVente> ligneVentes) {
		this.ligneVentes = ligneVentes;
	}

	public void setIdVente(Long idVente) {
		this.idVente = idVente;
	}

	public Long getIdVente() {
		return idVente;
	}

	public void setIdArticle(Long id) {
		this.idVente = id;
	}
}
