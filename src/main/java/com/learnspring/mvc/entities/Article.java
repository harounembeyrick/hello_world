package com.learnspring.mvc.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Article implements Serializable{

	@Id
	@GeneratedValue
	private Long idArticle;
	private String CodeArticle;
	private String designation;
	private BigDecimal prixunitaireHT;
	private BigDecimal tauxTva;
	private BigDecimal prixunitaireTTC;
	private String photo;
	
	@ManyToOne
	@JoinColumn(name="idCategory")
	private Category category;
	

	public Article() {}
	public String getCodeArticle() {
		return CodeArticle;
	}

	public void setCodeArticle(String codeArticle) {
		CodeArticle = codeArticle;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public BigDecimal getPrixunitaireHT() {
		return prixunitaireHT;
	}

	public void setPrixunitaireHT(BigDecimal prixunitaireHT) {
		this.prixunitaireHT = prixunitaireHT;
	}

	public BigDecimal getTauxTva() {
		return tauxTva;
	}

	public void setTauxTva(BigDecimal tauxTva) {
		this.tauxTva = tauxTva;
	}

	public BigDecimal getPrixunitaireTTC() {
		return prixunitaireTTC;
	}

	public void setPrixunitaireTTC(BigDecimal prixunitaireTTC) {
		this.prixunitaireTTC = prixunitaireTTC;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public void setIdArticle(Long idArticle) {
		this.idArticle = idArticle;
	}

	public Long getIdArticle() {
		return idArticle;
	}

	public void setIdArticle(long idArticle) {
		this.idArticle = idArticle;
	}
}
